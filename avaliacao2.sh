#!/bin/bash

clear
menu() {
echo "Deseja usar um arquivo zip existente ou criar um novo? "
echo "1) Usar arquivo"
echo "2) Criar arquivo zip"
}

options() {
echo "E agora o que deseja fazer com esse arquivo?: "
echo '1) listar conteúdo'
echo '2) pré visualizar arquivo'	
echo '3) adicionar arquivo'
echo '4) remover arquivo'
echo '5) extrair'
echo '6) extrair arquivo em particular'
echo '7) sair do script'
}

while [ "$opt" != '7' ]; do
	menu
	read -p 'Selecione opção: ' opcao

	case $opcao in 
		1) echo "Lista de arquivos zip existentes:";
			ls *.zip;
			read -p "Digite o nome do arquivo desejado: ";;
		
		2) read -p "Qual o nome do novo arquivo zip?:  " nome_zip;
		read -p "Vamos criar um arquivo para colocar no Zip, Nome: " file;
		touch $file;
		zip "$nome_zip" "$file" ;;
		*) echo "selecione uma opção válida (1-2)"
			continue ;;
	esac

	options
	read -p 'Selecione opção (1-7): ' opt
	case $opt in
		1) zipinfo "$nome_zip";;
		2) zipinfo "$nome_zip";
			read -p "nome do arquivo a ser visualisado" view;
			unzip -l "$nome_zip" "$view" | less ;;
		3)ls -p | grep -v /;
			read -p "digite o nome do arquivo que deseja adicionar ao zip: " arq ;
			if [ -f "$arq" ]; then
				zip -u "$nome_zip" "$arq"
			else
				echo "arquivo inexistente digite um nome válido"
				continue
			fi;;
		4) zipinfo "$nome_zip";
			read -p "nome arquivo pra remover: " rem ;
			zip -d "$nome_zip" "$rem" ;;
		5) unzip "$nome_zip";;
		6) unzip -l "$nome_zip";
			read -p "nome do(s) arquivo(s) que deseja extrair: " ziprem
			unzip "$nome_zip" "$ziprem";;
		7) exit ;;
		*) echo "digite 7 para sair";;
	esac

done
